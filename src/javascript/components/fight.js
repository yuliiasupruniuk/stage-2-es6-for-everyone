import { controls } from '../../constants/controls';

function attack(attacker, defender) {
  let damage = attacker.isBlock ? 0 : defender.isBlock ? 0 : getDamage(attacker, defender);
  let damagePercent = (damage * 100) / defender.health;

  return defender.changedHealthIndicator(damagePercent);
}

function changeHealthBar(position, defender) {
  const fighterIndicator = document.querySelector(`#${position}-fighter-indicator`);
  if (defender.healthIndicator > 0) {
    fighterIndicator.style.width = `${defender.healthIndicator}%`;
  } else {
    fighterIndicator.style.width = 0;
  }
}

function isCriticalKeys(arr, key, playerCombination) {
  if (arr.length <= 3 && playerCombination.includes(key)) {
    arr.push(key);
  }
  setTimeout(() => (arr.length = 0), 10000);
  return arr.length;
}

function critAttack(attacker, defender) {
  let damage = attacker.isBlock ? null : getDamage(attacker, defender) * 2;
  let damagePercent = (damage * 100) / defender.health;

  return defender.changedHealthIndicator(damagePercent);
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const state = {
      firstFighter: {
        ...firstFighter,
        isBlock: false,
        healthIndicator: 100,
        critKeys: [],
        changedHealthIndicator(damage) {
          return (this.healthIndicator = this.healthIndicator - damage);
        }
      },

      secondFighter: {
        ...secondFighter,
        isBlock: false,
        healthIndicator: 100,
        critKeys: [],
        changedHealthIndicator(damage) {
          return (this.healthIndicator = this.healthIndicator - damage);
        }
      }
    };

    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup', onKeyUp);

    function onKeyDown(event) {
      switch (event.code) {
        case controls.PlayerOneAttack:
          attack(state.firstFighter, state.secondFighter);
          changeHealthBar('right', state.secondFighter);
          break;

        case controls.PlayerTwoAttack:
          attack(state.secondFighter, state.firstFighter);
          changeHealthBar('left', state.firstFighter);
          break;

        case controls.PlayerOneBlock:
          state.firstFighter.isBlock = true;
          break;

        case controls.PlayerTwoBlock:
          state.secondFighter.isBlock = true;
          break;

        default:
          break;
      }

      let firstCriticalAttack = isCriticalKeys(
        state.firstFighter.critKeys,
        event.code,
        controls.PlayerOneCriticalHitCombination
      );
      if (firstCriticalAttack === 3) {
        critAttack(state.firstFighter, state.secondFighter);
        changeHealthBar('right', state.secondFighter);
      }

      let secondCriticalAttack = isCriticalKeys(
        state.secondFighter.critKeys,
        event.code,
        controls.PlayerTwoCriticalHitCombination
      );
      if (secondCriticalAttack === 3) {
        critAttack(state.secondFighter, state.firstFighter);
        changeHealthBar('left', state.firstFighter);
      }

      if (state.firstFighter.healthIndicator <= 0) resolve(secondFighter);
      if (state.secondFighter.healthIndicator <= 0) resolve(firstFighter);
    }

    function onKeyUp(event) {
      switch (event.code) {
        case controls.PlayerOneBlock:
          state.firstFighter.isBlock = false;
          break;
        case controls.PlayerTwoBlock:
          state.secondFighter.isBlock = false;
          break;

        default:
          break;
      }
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender.defense);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(defense) {
  // return block power
  let dodgeChance = Math.random() + 1;
  return defense * dodgeChance;
}
