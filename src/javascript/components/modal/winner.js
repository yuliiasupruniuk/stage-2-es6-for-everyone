import { showModal } from './modal.js';

export function showWinnerModal(fighter) {
  // call showModal function
  const title = `Winner: ${fighter.name}`;
  const bodyElement = 'To start new game you need to press X button';
  const onClose = () => window.location.reload();
  showModal({ title, bodyElement, onClose });
}
