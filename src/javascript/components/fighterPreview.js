import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    let info = Object.entries(fighter);

    fighterElement.append(createFighterImage(fighter));

    info
      .filter((entity) => entity[0] !== '_id' && entity[0] !== 'source')
      .forEach((entity) => {
        const feature = createElement({ tagName: 'span' });
        feature.innerText = `${entity[0]}:${entity[1]}`;
        fighterElement.append(feature);
      });
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
